<?php

namespace Modules\Order\Models;

//use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Models\Tenant\ModelTenant;

class Gviaje extends ModelTenant
{

  //  protected $with = ['identity_document_type'];
 
    protected $fillable = [
        'user_id',
        'order_id',
        'descripcion',
        'tipo',
        'monto',
        'fecha',
        'documento_id',
        'galones',
        'precio',
        'filename',
        
        
    ];


   // public function identity_document_type()
   // {
       // return $this->belongsTo(IdentityDocumentType::class, 'identity_document_type_id');
   // }

}
