<?php
namespace Modules\Order\Http\Controllers;

use Modules\Order\Http\Requests\RutaRequest;
use Modules\Order\Http\Resources\RutaCollection;
use Modules\Order\Http\Resources\RutaResource;
use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Http\Controllers\Controller;
use Modules\Order\Models\Ruta;
use Illuminate\Http\Request;

class RutaController extends Controller
{

    public function index()
    {
        return view('order::rutas.index');
    }

    public function columns()
    {
        return [
            'descripcion' => 'Descripcion',
            
        ];
    }

    public function records(Request $request)
    {

        $records = Ruta::where($request->column, 'like', "%{$request->value}%")
                            ->orderBy('descripcion');

        return new RutaCollection($records->paginate(config('tenant.items_per_page')));
    }


    public function tables()
    {
        $identity_document_types = IdentityDocumentType::whereActive()->get();
        $api_service_token = config('configuration.api_service_token');

        return compact('identity_document_types', 'api_service_token');
    }

    public function record($id)
    {
        $record = new RutaResource(Ruta::findOrFail($id));

        return $record;
    }

    public function store(RutaRequest $request)
    {

        $id = $request->input('id');
        $record = Ruta::firstOrNew(['id' => $id]);
        $record->fill($request->all());
        $record->save();

        return [
            'success' => true,
            'message' => ($id)?'Ruta editado con éxito':'Ruta registrado con éxito',
            'id' => $record->id
        ];
    }

    public function destroy($id)
    {

        $record = Ruta::findOrFail($id);
        $record->delete();

        return [
            'success' => true,
            'message' => 'Ruta eliminado con éxito'
        ];

    }

}
