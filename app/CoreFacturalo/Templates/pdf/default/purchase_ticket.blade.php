@php
    $establishment = $document->establishment;
    $supplier = $document->supplier;
    
    $tittle =str_pad($document->id, 8, '0', STR_PAD_LEFT); 
@endphp
<html>
<head>
    {{--<title>{{ $tittle }}</title>--}}
    
    {{-- <style>
        .ticket {
            width: 283px;
            max-width: 280px;
        }
    </style>--}}
</head>
<body>
<div class="ticket">
<table class="full-width">
    <tr>
        @if($company->logo)
            <td width="20%">
                <div class="company_logo_box">
                    <img src="data:{{mime_content_type(public_path("storage/uploads/logos/{$company->logo}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/logos/{$company->logo}")))}}" alt="{{$company->name}}" class="company_logo" style="max-width: 150px;">
                </div>
            </td>
        @else
            <td width="20%">
                {{--<img src="{{ asset('logo/logo.jpg') }}" class="company_logo" style="max-width: 150px">--}}
            </td>
        @endif
        <td width="50%" class="pl-3">
            <div class="text-left">
                <h4 class="">{{ $company->name }}</h4>
                <h5>{{ 'RUC '.$company->number }}</h5>
                
            </div>
        </td>
        <td width="30%" class="border-box py-4 px-2 text-center">
            <h5 class="text-center">{{ $document->document_type->description}}</h5>
            <h3 class="text-center">{{ $tittle }}</h3>
        </td>
    </tr>
</table>
<table class="full-width mt-5">
    <tr>
        <td width="15%">Proveedor:</td>
        <td width="45%">{{ $supplier->name }}</td>
        <td width="15%">Fecha:</td>
        <td width="15%">{{ $document->date_of_issue->format('Y-m-d') }}</td>
    </tr>
    <tr>
        <td>{{ $supplier->identity_document_type->description }}:</td>
        <td>{{ $supplier->number }}</td>
        @if($document->date_of_due)
            <td width="25%">Fecha de vencimiento:</td>
            <td width="15%">{{ $document->date_of_due->format('Y-m-d') }}</td>
        @endif
    </tr>
    @if ($supplier->address !== '')
    <tr>
        <td class="align-top">Dirección:</td>
        <td colspan="3">
            {{ $supplier->address }}
            {{ ($supplier->district_id !== '-')? ', '.$supplier->district->description : '' }}
            {{ ($supplier->province_id !== '-')? ', '.$supplier->province->description : '' }}
            {{ ($supplier->department_id !== '-')? '- '.$supplier->department->description : '' }}
        </td>
    </tr>
    @endif 
    @if ($supplier->telephone)
    <tr>
        <td class="align-top">Teléfono:</td>
        <td colspan="3">
            {{ $supplier->telephone }}
        </td>
    </tr>
    @endif 
    <tr>
        <td class="align-top">Usuario:</td>
        <td colspan="3">
            {{ $document->user->name }}
        </td>
    </tr>
    <tr>
        <td class="align-top">Placa:</td>
        <td colspan="3">
            {{ $document->placa }}
        </td>
    </tr>
   
</table>
 

<table class="full-width mt-10 mb-10">
    <thead class="">
    <tr class="bg-grey">
        <th class="border-top-bottom text-center py-2" width="15%">CANT.</th>
        
        <th class="border-top-bottom text-left py-2" width="50%">DESCRIPCIÓN</th>
        <th class="border-top-bottom text-right py-2" width="19%">P.UNIT</th>
        
        <th class="border-top-bottom text-right py-2" width="19%">TOTAL</th>
    </tr>
    </thead>
    <tbody>
    @foreach($document->items as $row)
        <tr>
            <td class="text-center align-top">
                @if(((int)$row->quantity != $row->quantity))
                    {{ $row->quantity }}
                @else
                    {{ number_format($row->quantity, 0) }}
                @endif
            </td>
            <td class="text-right align-top">{{ $row->description}}</td>
            <td class="text-right align-top">{{ number_format($row->unit_price, 2) }}</td>
            
            <td class="text-right align-top">{{ number_format($row->total, 2) }}</td>
        </tr>
        <tr>
            <td colspan="6" class="border-bottom"></td>
        </tr>
    @endforeach
       
    </tbody>
</table>

<table class="full-width" style="margin-top:60px">
    <tr>
         
            <td colspan="6" class="border-bottom"></td>
        
    </tr>
    <tr>
         
            <td colspan="6" style="margin-left:20px"><b> FIRMA DE PROVEEDOR</b></td>
        
    </tr>
</table>
</div>
</body>
</html>
